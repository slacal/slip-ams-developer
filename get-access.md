# Getting API Access 

## Generate a Key
A user must have the "View My Settings" permission to get an API key. If your SLIP user doesn't have that permission, speak with your brokerage's Master user.

1. Login to SLIP at [slip.slacal.com](https://slip.slacal.com)

    - If you are a part of multiple brokerages, you must select the brokerage for which you intent to submit AMS data.

2. Click on Settings tab

    ![Click the Settings tab](./images/get-access1.png)

    *Or to generate a key for a user other than the currently logged in user, select that user from the user management screen.*

3. Click to Regenerate AMS Token
    
    ![Regenerate AMS Token](./images/get-access2.png)


## Environments

In addition to the Production environment for live data, the SLA also provides a testing environment for use during AMS integration development.

| Environment | SLIP Url | SLIP AMS Base Url |
|---|---|---|
| Production | https://slip.slacal.com | https://slip-api.slacal.com |
| Test | https://slip.train.slacal.com | https://slip-api.train.slacal.com |

To gain access to testing environment APIs, email [support@slacal.com](mailto:support@slacal.com) the following information:

- Name
- Phone Number
- Email

## What's Next?

The SLA has provided the following resources to assist developers in implementing integration with SLIP AMS.

| Resource | Description |
| --- | --- |
| [Explore the API](https://slip-api.slacal.com/docs/index) | Explore the available API operations. View the [batch submission schema](https://slip-api.slacal.com/api/schema). |
| [Example C# Client](https://bitbucket.org/slacal/slip-ams-client/) | Run the sample client to connect to SLIP AMS. Use the source code and the [sample batch submission data](https://bitbucket.org/slacal/slip-ams-client/src/master/SLACAL.SLIP.AMS.Client/samples/) as an example when implementing an integration with SLIP AMS for you organization's AMS. |

## Legacy SLIP AMS

The steps and resources described on this page apply to the latest version of SLIP AMS. For users of the legacy XML-based AMS, documentation is available [here](./xml/legacy-readme.md).

**Note:** SLA California intends to sunset the legacy XML-based AMS, but will not do so until a transition plan is built for the current consuming brokerages. 