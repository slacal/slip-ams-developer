# SLIP AMS Developer

## What is it?
SLIP AMS allows an authorized broker to electronically submit their policy information to [The Surplus Line Association of California (the SLA)](https://www.slacal.com/) by generating a batch file from their internal Agency Management System (AMS) and submitting it using web services provided by SLIP. 

## Who should use this repository?
This repository is for developers and teams that wish to understand the details of how to electronically submit policy information to the SLA using either Bulk Upload (XML/JSON) or AMS Upload.

## Questions & Help
For any questions or help, please [send us a message](https://www.slacal.com/send-us-a-message).

## What's Next?
To get started submitting your brokerage's surplus lines information, [get access to the API](get-access.md).

## 2024 SL2 Form Updates

As of January 1, 2024, the SLA is using a new format and layout for the SL2 Form. Please note that while we are providing an updated JSON schema to reflect these changes, Users can continue to provide filings using the old version of the JSON schema, and the data will be converted to the new SL2 Form format automatically. **No changes are required** to continue submitting the old version of the JSON schema.

**If you wish to update your schema to reflect the new SL2 format, the SLA encourages you to update to the new JSON schema.** If you would like more information regarding switching to the new JSON schema, reach out to [support@slacal.com](mailto:support@slacal.com) and we will be happy to assist you.

## Legacy SLIP AMS

For users of the legacy XML-based AMS, documentation is available [here](./xml/legacy-readme.md).

**Note:** The SLA intends to sunset the legacy XML-based SLIP AMS, but will not do so until a transition plan is built for the current consuming brokerages. 

## References

* [SLIP AMS Overview](https://www.slacal.com/slip-ams)
* [Explore the API](https://slip-api.slacal.com/docs/index)
* [Example C# Client](https://bitbucket.org/slacal/slip-ams-client/)
* [Check Transaction Status API](https://bitbucket.org/slacal/slip-ams-developer/src/master/xml/lib/checktransactionstatus-api-doc.md)