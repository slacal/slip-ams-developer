import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.slacal.ws.schema.slip_batch.Address;
import org.slacal.ws.schema.slip_batch.ArrayOfCoverage;
import org.slacal.ws.schema.slip_batch.ArrayOfExportOrCoverageCode;
import org.slacal.ws.schema.slip_batch.ArrayOfFee;
import org.slacal.ws.schema.slip_batch.ArrayOfPolicy;
import org.slacal.ws.schema.slip_batch.ArrayOfSL1NonAdmittedInsurer;
import org.slacal.ws.schema.slip_batch.ArrayOfSL2AdmittedInsurers;
import org.slacal.ws.schema.slip_batch.ArrayOfTransaction;
import org.slacal.ws.schema.slip_batch.Batch;
import org.slacal.ws.schema.slip_batch.BatchDataSet;
import org.slacal.ws.schema.slip_batch.Broker;
import org.slacal.ws.schema.slip_batch.Coverage;
import org.slacal.ws.schema.slip_batch.DocumentDetail;
import org.slacal.ws.schema.slip_batch.DocumentDetail.DocumentType;
import org.slacal.ws.schema.slip_batch.ExportOrCoverageCode;
import org.slacal.ws.schema.slip_batch.Fee;
import org.slacal.ws.schema.slip_batch.Fee.FeeType;
import org.slacal.ws.schema.slip_batch.FolderOrDocuments;
import org.slacal.ws.schema.slip_batch.Insurer;
import org.slacal.ws.schema.slip_batch.InsuredContact;
import org.slacal.ws.schema.slip_batch.Licensee;
import org.slacal.ws.schema.slip_batch.ObjectFactory;
import org.slacal.ws.schema.slip_batch.Policy;
import org.slacal.ws.schema.slip_batch.SL1Form;
import org.slacal.ws.schema.slip_batch.SL1NonAdmittedInsurer;
import org.slacal.ws.schema.slip_batch.SL2Form;
import org.slacal.ws.schema.slip_batch.SL2AdmittedInsurer;
import org.slacal.ws.schema.slip_batch.SL2AdmittedInsurer.RepresentativeType;
import org.slacal.ws.schema.slip_batch.SL2AdmittedInsurer.DeclinationCode;
import org.slacal.ws.schema.slip_batch.Transaction;
import org.slacal.ws.schema.slip_batch.Transaction.TransactionType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;

/**
 * Sample class demonstrating the use of the AMS Batch Submission API for SLIP
 * It was build with the JAXB XML data binding framework for Java
 */
public class BatchSample
{
	// Get ObjectFactory for SLIP_BATCH classes
	private static ObjectFactory of = new ObjectFactory();

	public static void main(String args[])
	{
		// Separate list to track all documents for the entire batch
		List<DocumentDetail> alldocs = new ArrayList<DocumentDetail>();

		// Broker Details (for Batch and Policy)
		Broker broker = new Broker();
		broker.setBrokerName("William Eakin");
		broker.setLicenseNumber("SL0123456");
		broker.setSLABrokerNumber(9123);

		// Batch Coversheet (add to batch and alldocs)
		DocumentDetail dd = new DocumentDetail();
		dd.setCustomDocumentId("My first batch coversheet");
		dd.setDocumentName("batchcover.pdf");
		dd.setDocumentType(DocumentType.COVER_NOTE);
		dd.setXmlDocumentId(BigInteger.valueOf(0));
		FolderOrDocuments fod_batch = new FolderOrDocuments();
		fod_batch.getFolderNameOrDocument().add(dd);
		alldocs.add(dd);

		// Policy Document Images (add to policy and alldocs)
		dd = new DocumentDetail();
		dd.setCustomDocumentId("My first policy");
		dd.setDocumentName("itemimages.pdf");
		dd.setDocumentType(DocumentType.MULTIPLE);
		dd.setXmlDocumentId(BigInteger.valueOf(1));
		FolderOrDocuments fod_policy = new FolderOrDocuments();
		fod_policy.getFolderNameOrDocument().add(dd);
		alldocs.add(dd);

		// Insured Contact
		InsuredContact ic = new InsuredContact();
		ic.setName("Troppo Buono Cafe");
		Address ic_addr = new Address();
		ic_addr.setAddressLine1("1690 Golden Gate Ave");
		ic_addr.setCity("San Francisco");
		ic_addr.setStateOrProvince("CA");
		ic_addr.setPostalCode("94115");
		ic.setAddress(ic_addr);

		// Insurer
		Insurer insurer = new Insurer();
		insurer.setName("Laura Ferruccio Insurance Company");
		insurer.setNAICNumber("91234");

		// Coverage(s)
		Coverage cov = new Coverage();
		cov.setCoverageCode(BigInteger.valueOf(994));
		cov.setCoveragePremium(BigDecimal.valueOf(5100.00));
		cov.setInsurerOrLayerGroup(insurer);
		// Array/List of Coverages
		ArrayOfCoverage aoc = new ArrayOfCoverage();
		aoc.getCoverage().add(cov);

		// Fees
		Fee broker_fee = new Fee();
		broker_fee.setFeeAmount(BigDecimal.valueOf(100.00));
		broker_fee.setFeeType(FeeType.BROKER_FEE);
		// not taxable if imposed by broker (see bulletin 997)
		broker_fee.setIncludeInPremium(false);
		Fee policy_fee = new Fee();
		policy_fee.setFeeAmount(BigDecimal.valueOf(100.00));
		policy_fee.setFeeType(FeeType.POLICY_FEE);
		// taxable if imposed by insurer (see bulletin 997)
		policy_fee.setIncludeInPremium(true);
		// Array/List of Fees
		ArrayOfFee aof = new ArrayOfFee();
		aof.getFee().add(broker_fee);
		aof.getFee().add(policy_fee);


		// SL1 Form
		SL1Form sl1 = new SL1Form();
		sl1.setPolicyNumber("WEY67M5D16");
		sl1.setCaliforniaPremium(BigDecimal.valueOf(5200.00));
		sl1.setCoverageOnExportListOrDiligentSearchReport(
			of.createSL1FormDiligentSearchReport("Yes"));
		sl1.setBrokerName("William Eakin");
		Licensee lic = new Licensee();
		lic.setName("W. Eakin Co, Inc");
		lic.setLicenseNumber("0123456");
		sl1.setBrokerLicenseNumberOrOrganization(lic);
		sl1.setDateSigned(new GregorianCalendar(2003,5,14));
		ExportOrCoverageCode eocc = new ExportOrCoverageCode();
		eocc.setCoverageCode(BigInteger.valueOf(994));
		ArrayOfExportOrCoverageCode aoeocc = new ArrayOfExportOrCoverageCode();
		aoeocc.getExportOrCoverageCode().add(eocc);
		sl1.setExportOrCoverageCodes(aoeocc);
		sl1.setInsured(ic);
		sl1.setRiskLocation(ic_addr);
		sl1.setRiskDescription("Coffee Shop");
		SL1NonAdmittedInsurer sl1ins = new SL1NonAdmittedInsurer();
		sl1ins.setPremiumPercentage(BigDecimal.valueOf(100.0));
		sl1ins.setNonAdmittedInsurerName("Laura Ferruccio Insurance Company");
		sl1ins.setNAICNumber("91234");
		ArrayOfSL1NonAdmittedInsurer aosl1ins = new ArrayOfSL1NonAdmittedInsurer();
		aosl1ins.getSL1NonAdmittedInsurer().add(sl1ins);
		sl1.setNonAdmittedInsurers(aosl1ins);

		// SL2 Form
		SL2Form sl2 = new SL2Form();
		sl2.setBrokerName("Ginger Oxworth");
		sl2.setBrokerLicenseNumberOrOrganization(lic);
		sl2.setDateSigned(new GregorianCalendar(2003,5,14));
		sl2.setExportOrCoverageCodes(aoeocc);
		sl2.setInsured(ic);
		sl2.setRiskLocation(ic_addr);
		sl2.setRiskDescription("Coffee Shop");
		sl2.setDiligentEfforts("Done nothing - SL2 Form 6(A) is blank");
		sl2.setNameOfSearchPerformer("William Eakin");
		sl2.setWasSubmittedToAdmittedInsurers(true);
		ArrayOfSL2AdmittedInsurers aosl2ins = new ArrayOfSL2AdmittedInsurers();
		SL2AdmittedInsurer ai1 = new SL2AdmittedInsurer();
		ai1.setCompanyName("Triple Delta Insurance, Ltd");
		ai1.setNAICNumber("91111");
		ai1.setRepresentativeName("Colleen Farley");
		ai1.setRepresentativePhone("(415) 555-1212");
		ai1.setRepresentativeType(RepresentativeType.EMPLOYEE);
		ai1.setDeclinationMonth(Integer.valueOf(1));
		ai1.setDeclinationYear("2003");
		ai1.setDeclinationCode(DeclinationCode.CAPACITY_REACHED);
		aosl2ins.getSL2AdmittedInsurer().add(ai1);
		SL2AdmittedInsurer ai2 = new SL2AdmittedInsurer();
		ai2.setCompanyName("Seashore Partners Insurance Co");
		ai2.setNAICNumber("92222");
		ai2.setRepresentativeName("Sudha Biswah");
		ai2.setRepresentativePhone("(714) 555-1212");
		ai2.setRepresentativeType(RepresentativeType.AGENT);
		ai2.setDeclinationMonth(Integer.valueOf(1));
		ai2.setDeclinationYear("2003");
		ai2.setDeclinationCode(DeclinationCode.REFUSED_TO_STATE);
		aosl2ins.getSL2AdmittedInsurer().add(ai2);
		SL2AdmittedInsurer ai3 = new SL2AdmittedInsurer();
		ai3.setCompanyName("Southern State Underwriting, LLC");
		ai3.setNAICNumber("93333");
		ai3.setRepresentativeName("Joseph Pirlo");
		ai3.setRepresentativePhone("(213) 555-1212");
		ai3.setRepresentativeType(RepresentativeType.EMPLOYEE);
		ai3.setDeclinationMonth(Integer.valueOf(2));
		ai3.setDeclinationYear("2003");
		ai3.setDeclinationCode(DeclinationCode.CAPACITY_REACHED);
		aosl2ins.getSL2AdmittedInsurer().add(ai3);
		sl2.setAdmittedInsurers(aosl2ins);


		// Transaction
		Transaction t = new Transaction();
		t.setXmlTransactionId(BigInteger.valueOf(1));
		t.setTransactionType(TransactionType.N); // New Business
		// 5800 = Eating and Drinking Places
		t.setSICCodeOrNAICSCode(of.createTransactionSICCode("5800"));
		t.setPremium(BigDecimal.valueOf(5200.00));
		t.setFees(aof);
		t.setCoverages(aoc);
		t.setDocuments(fod_policy);
		t.setSL1(sl1);
		t.setSL2(sl2);
		t.setLateFilingExplanation("Should have done that long time ago but didn't have the time to do it then.");
		// Array/List of Transactions
		ArrayOfTransaction aot = new ArrayOfTransaction();
		aot.getTransaction().add(t);

		// Policy
		Policy p = new Policy();
		p.setBroker(broker);
		p.setPolicyNumber("WEY67M5D16");
		p.setXmlPolicyId(BigInteger.valueOf(1));
		p.setInsured(ic);
		p.setTransactions(aot);
		p.setEffectiveDate(new GregorianCalendar(2003,5,16));
		p.setExpirationDate(new GregorianCalendar(2004,5,16));
		p.setIsMultipleStatePolicy(false);
		p.setIsMasterPolicy(false);
		// Array/List of Policies (just one in this sample)
		ArrayOfPolicy aop = new ArrayOfPolicy();
		List<Policy> lp = aop.getPolicy();
		lp.add(p);

		// Batch
		Batch batch = new Batch();
		batch.setBroker(broker);
		batch.setBrokerBatchDate(new GregorianCalendar(2003,5,15));
		batch.setBrokerBatchNumber("My first sample batch");
		batch.setPolicies(aop);
		batch.setBatchNotes("My first sample batch");
		batch.setDocuments(fod_batch);
		// Batch Data Set
		BatchDataSet bds = new BatchDataSet();
		bds.setReportingState("CA");
		bds.setBatch(batch);

		// Create the Batch ZIP file (includes XML)
		createZIP("batch.zip",bds,alldocs);

		// Create the XML separately and display it
		createXML(bds, System.out);
	}

	/**
	 * createXML turns a BatchDataSet into an XML datastream
	 *
	 * @param bds
	 * 	The BatchDataSet with all the data to be submitted to the SLA
	 *
	 * @param os
	 * 	The OutputStream to which the XML data is being written. This
	 * 	can be a FileOutputStream to write a .xml file or a ZipOutputStream
	 * 	to create an entry in a .zip file or it can be System.out to
	 * 	display the generated XML.
	 */
	public static void createXML(BatchDataSet bds, OutputStream os)
	{
		try
		{
			// Get a JAXBContext for SLIP_BATCH
			JAXBContext jc = JAXBContext.newInstance("org.slacal.ws.schema.slip_batch");
			// Wrap BatchDataSet POJO into JAXBElement
			// Note: needed unless the class is annotated XmlRootElement
			JAXBElement root = of.createBatchDataSet(bds);
			// Create a JAXB Marshaller
			Marshaller m = jc.createMarshaller();
			// Make the XML pretty
			// Note: important for meaningful error messages
			m.setProperty("jaxb.formatted.output",true);
			// Produce the XML for our data
			m.marshal(root, os);
			os.flush();
		}
		catch(Exception ex)
		{
			System.err.println("createXML: " + ex);
		}
	}

	/**
	 * createZIP produce the ZIP file for SLIP Batch upload
	 * 	createZIP calls createXML to produce the XML datafile that
	 * 	will be included in the ZIP file. It uses the information
	 * 	in the alldocs list to add the document image files.
	 *
	 * @param zipfilename
	 * 	The name of the ZIP file to generate (must have .zip suffix)
	 *
	 * @param bds
	 * 	The BatchDataSet with all the data to be submitted to the SLA
	 *
	 * @param alldocs
	 * 	The list of DocumentDetails contains all the document image
	 * 	files that need to be included in the ZIP file.
	 */
	public static void createZIP(String zipfilename, BatchDataSet bds, List<DocumentDetail> alldocs)
	{
		try
		{
			// Create a new Zipfile
			ZipOutputStream zos = new ZipOutputStream(
				new FileOutputStream(zipfilename, false));
			zos.setComment("Generated by BatchSample.java");

			// Call createXML to add the XML data as "batch.xml"
			zos.putNextEntry(new ZipEntry("batch.xml"));
			createXML(bds,zos);
			zos.closeEntry();

			// Go through the list of documents and add them
			for(DocumentDetail dd : alldocs)
			{
				String filename = dd.getDocumentName();
				File doc = new File(filename);
				if(doc.exists() && doc.isFile() && doc.canRead())
				{
					byte[] data = new byte[(int)doc.length()];
					FileInputStream fis = new FileInputStream(doc);
					fis.read(data);
					fis.close();
					zos.putNextEntry(new ZipEntry(filename));
					zos.write(data);
					zos.closeEntry();
				}
				else
				{
					System.err.println("Document '" + filename +
						"' does not exist or is not readable!");
				}
			}

			// Finalize the Zipfile
			zos.flush();
			zos.close();
		}
		catch(Exception ex)
		{
			System.err.println("createZIP: " + ex);
		}
	}
}
