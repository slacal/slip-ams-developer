# Java Example Programs for SLIP AMS Batch Submission

- **`BatchSample`** program shows how to use Java to automatically create a valid
batch.zip file containing XML data and Document Image files for upload.

- **`AMSsample`** program shows how to upload a SLIP batch.zip file using the
SLIP AMS webservices as well as how to check the status and get the
notifications for previously uploaded batches.

#### The two example programs only require the Sun/Oracle Java SDK.

>They were built using the JAX-WS (Java API for XML WebServices) for the SLIP
AMS webservices and JAXB (Java Architecture for XML Binding) for the SLIP
Batch XML data binding but the versions of those toolkits/frameworks that are
included with the 1.6.* Java SDK are sufficient to build and run the samples.

>JAX-WS and JAXB are part of WSIT (WebServices Interoperability Technology)
and are the reference implementations from Sun/Oracle are available for
free under the CDDL Open Source License.

---

## Step 1
Ensure that you have your executable search PATH includes the `JAVA_HOME/bin` directory.

The first of the following commands sets the
PATH environment variable if it is not already set. The next 4 lines run
the 4 executables with the option `-version` to see if you can successfully
execute them.

```
export PATH=$JAVA_HOME/bin:$PATH

java -version

javac -version

xjc -version

wsimport -version
```


## Step 2
Create Java classes for the SLIP Batch XML schema.

This is done using the tool `xjc` (part of JAXB) by specifying the URL of the
SLACAL_AMS.xsd file on the main SLA website.
The binding file `slip_batch.xjb` is used to override default behavior of the
`xjc` schema compiler to produce more convenient Java classes for our purposes.
We are also including an optional extra schema `xmlschema.xsd` that is not
strictly necessary for the sample programs but may be important when building
a more complex application. It ensures that the xmladapter class generated
by `xjc` is in the same namespace as the rest of the classes.
The following command assumes that your current working directory is the
directory "samples" with the source code for the two example programs.
The target directory for the generated source code (`src`) must exist or the
command will fail. If necessary create it with `mkdir src`.
The `xjc` tool only generates the source code for the Java classes and we need
the second command javac to compile the generated Java code.

```
xjc -b slip_batch.xjb -d src xmlschema.xsd "http://www.slacal.org/docs/default-source/general-content-documents/Batch/brokerage-xml-schema-(xsd).xsd?sfvrsn=0"

javac -d . `find src -name "*.java" -print`
```


## Step 3
Create Java classes for the SLIP AMS WebService

This is done using the tool `wsimport` (part of JAX-WS) by specifying the URL
of the WSDL (WebService Definition Language) file. By convention, the WSDL
for a webservice can be obtained by adding the suffix `?wsdl` to the URL of
the service endpoint.
The binding file `slip_wsapi.xjb` is used to override default behavior of the
wsimport tool to produce more convenient Java classes for our purposes.
The `-extension` option is needed due to non-standard features in the WSDL.
The `-XadditionalHeaders` option is needed to turn extra headers in the SOAP
message into parameters when calling the webservice API methods.
Once again the command assumes that your current working directory is the
directory "samples" with the source code for the two example programs.
The target directory for the generated source code (`src`) must exist or the
command will fail but it should already be there from the previous step.
Unlike the xjc compiler, the `wsimport` tool not only generates the Java source
but also compiles it which eliminates the need for separate compilation.

*Note: this step will produce some warnings but those can be ignored.*

```
wsimport -b slip_wsapi.xjb -extension -XadditionalHeaders -s src https://slipws-train.slacal.org/AmsBatchFiling.svc?wsdl
```


## Step 4 [Optional]
Create a JAR library file from the generated Java classes

While the generated Java classes can be used as they are after the previous
steps, you may want to package the generated Java classes in a JAR library
to make it easier to reuse them in your own projects. The `jar` utility is
packaging all the compiled Java classes into the jar file `slip_ams.jar`
If you want to use the jar file to compile or run Java code depending on the
generated Java classes you would set your CLASSPATH to include the jar file.
Otherwise if you don't want or need the jar file you can set your CLASSPATH
to include the samples directory and compiling or running Java applications
will work too.

```
jar -cvf slip_ams.jar com org
```


## Step 5 [Optional]
Create documentation for the generated Java classes

This optional but recommended step produces documentation for the all the
generated Java classes from the earlier steps as well as for the example
programs themselves. To view the documentation open doc/index.html in a 
browser.

```
javadoc -sourcepath .:src -d doc `find . -name "*.java" -print`
```


## Step 6
Compile the two example Java programs

This step compiles our two example programs. You may want to edit the two
Java source files (`AMSsample.java` and `BatchSample.java`) to match your own
SLIP user credentials for Testing/Training site at the SLA.

- In `BatchSample.java` change the SLA Broker Number in the Broker object.
- In `AMSsample.java` change the values in the AuthenticationHeader object. 

However the generic test account shown in the example code is functional
and you can run the example programs without any modification.

```
javac -cp slip_ams.jar:. BatchSample.java

javac -cp slip_ams.jar:. AMSsample.java
```


## Step 7
Run the `BatchSample` program to produce a SLIP batch.zip file

Just run BatchSample which will create a valid SLIP batch.zip file. It will
also display the generated XML data.

```
java -cp slip_ams.jar:. BatchSample
```


## Step 8
Run the AMSsample program

We are going to run the AMSsample multiple times. Without any commandline
arguments the program will perform a validation of the user credentials
and display a usage message.
- With the option `-vc` the AMSsample program will only perform the check of
the user credentials and exit.
- With the option `-ub batch.zip` the AMSsample program will upload a SLIP
batch.zip file produced by the BatchSample program but will not submit it.
- With the option `-sb batch.zip` the AMSsample program will upload a SLIP
batch.zip file produced by the BatchSample program and immediately submit it.
- With the option `-cs #` the AMSsample program will check the status of a
previously uploaded batch (use the actual batch submission number instead
of the #-sign).
- With the option `-gn #` the AMSsample program will get alert notifications
for a previously uploaded batch (use the actual batch submission number
instead of the #-sign).

*Note: depending on your Java version and optionally installed components you
may see some warnings while executing the webservices (WSP0019,WSP0075).*

```
java -cp slip_ams.jar:.  AMSsample

java -cp slip_ams.jar:.  AMSsample -vc

java -cp slip_ams.jar:.  AMSsample -ub batch.zip

java -cp slip_ams.jar:.  AMSsample -sb batch.zip

java -cp slip_ams.jar:.  AMSsample -cs 3085 

java -cp slip_ams.jar:.  AMSsample -gn 3085
```
