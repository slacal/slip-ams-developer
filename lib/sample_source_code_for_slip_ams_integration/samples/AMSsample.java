import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.List;
import javax.xml.namespace.QName;
import org.slacal.ws.test.ws_slip.AmsBatchFiling;
import org.slacal.ws.test.ws_slip.AMSBatchFilingSoap;
import org.slacal.ws.test.ws_slip.AMSBatchFilingMtom;
import org.slacal.ws.test.ws_slip.UploadBatchFilingStream;
import org.slacal.ws.test.ws_slip.messages.UploadBatchFilingStreamResponse;
import org.slacal.ws.test.ws_slip.types.ArrayOfSubmissionError;
import org.slacal.ws.test.ws_slip.types.ArrayOfNotification;
import org.slacal.ws.test.ws_slip.types.AuthenticationHeader;
import org.slacal.ws.test.ws_slip.types.CheckStatusResult;
import org.slacal.ws.test.ws_slip.types.ElementType;
import org.slacal.ws.test.ws_slip.types.GetNotificationsResult;
import org.slacal.ws.test.ws_slip.types.Notification;
import org.slacal.ws.test.ws_slip.types.NotificationStatus;
import org.slacal.ws.test.ws_slip.types.Status;
import org.slacal.ws.test.ws_slip.types.Type;
import org.slacal.ws.test.ws_slip.types.SubmissionError;
import org.slacal.ws.test.ws_slip.types.UploadBatchFilingResult;
import org.slacal.ws.test.ws_slip.types.VerifyCredentialsResult;

/**
 * Sample class demonstrating the use of the AMS Batch Submission API for SLIP
 * It was build with the JAX-WS webservice framework for Java
 */
public class AMSsample
{
	/**
	 * main
	 * 	main method for the AMSsample webservice client  application
	 *
	 * @param args
	 * 	An array of strings representing the commandline arguments
	 * 	passed to the program at runtime. The supported arguments are:
	 * 	-vc               (run VerifyCredentials
	 * 	-ub <batch.zip>   (run UploadBatchFiling)
	 * 	-cs <submission#> (run CheckStatus)
	 * 	-gn <submission#> (run GetNotifications)
	 */
	public static void main(String[] args)
	{
		try
		{
			// create client service using the default constructor
			AmsBatchFiling service = new AmsBatchFiling();
			// Alternatively the service can be created by specifying the
			// the WSDL URL. This allows to generate the client
			// code only once and then run the same code in either the
			// Test/Training or the Production environment:
			//URL url = new URL("...?wsdl");
			//AmsBatchFiling service = new AmsBatchFiling(url);

			// V1.1: the syntax shown above only works with JAX-WS 2.2.x
			// For JAX-WS 2.1.x (which is included in JDK 1.6) a second
			// argument is required for the AmsBatchFiling() constructor:
			//URL url = new URL("https://trainws.slip.slacal.org/AmsBatchFiling.svc?wsdl");
			//QName svcname = new QName("http://test.ws.slacal.org/ws-slip/", "AmsBatchFiling");
			//AmsBatchFiling service = new AmsBatchFiling(url, svcname);

			// create all service endpoints / ports
			// (only 1 of the 2 SOAP endpoints is needed) but a separate
			// endpoint is specified for the UploadBatchFiling API using MTOM
			AMSBatchFilingSoap ep_soap11 = service.getAMSBatchFilingSoap();  //SOAP 1.1
			AMSBatchFilingSoap ep_soap12 = service.getAMSBatchFilingSoap12();//SOAP 1.2
			AMSBatchFilingMtom ep_mtom = service.getAMSBatchFilingMtom();    // MTOM


			// create AuthenticationHeader common to all SLIP services
			AuthenticationHeader ah = new AuthenticationHeader();
			// TODO: update the following 3 lines with your own account details!
			ah.setSLABrokerNumber("9123");
			ah.setUserName("TEST9123");
			ah.setAPIKey("9d04446f-c039-4250-9531-185f917a3ca6");

			// run VerifyCredentials (same test using SOAP 1.1 and SOAP 1.2)
			verifyCredentials(ep_soap11, ah);
			verifyCredentials(ep_soap12, ah);

			// Show usage information if no arguments are provided
			if(args.length == 0)
			{
				System.out.println("Usage: ");
				System.out.println("# To execute VerifyCredentials only:");
				System.out.println("java AMSsample -vc");
				System.out.println("# To execute UploadBatchFiling without submit:");
				System.out.println("java AMSsample -ub  <batch.zip>");
				System.out.println("# To execute UploadBatchFiling with submission:");
				System.out.println("java AMSsample -sb  <batch.zip>");
				System.out.println("# To execute CheckStatus:");
				System.out.println("java AMSsample -cs  <submission#>");
				System.out.println("# To execute GetNotifications:");
				System.out.println("java AMSsample -gn  <submission#>");
			}

			if(args.length == 2)
			{
				String submitnum = null;
				if(args[0].equalsIgnoreCase("-ub"))
				{
					// run UploadBatchFiling test using MTOM endpoint
					submitnum = uploadBatchFiling(ep_mtom, ah, args[1],true);
				}
				else if(args[0].equalsIgnoreCase("-sb"))
				{
					// run UploadBatchFiling test using MTOM endpoint
					submitnum = uploadBatchFiling(ep_mtom, ah, args[1],false);
				}
				else
				{
					submitnum = args[1];
				}
		
				if(args[0].equalsIgnoreCase("-cs"))
				{
					// run CheckStatus for given submitnum
					checkStatus(ep_soap12, ah, submitnum);
					// run GetNotification for same submitnum
					getNotifications(ep_soap12, ah, submitnum);
				}
			
				if(args[0].equalsIgnoreCase("-gn"))
				{
					// run GetNotification
					getNotifications(ep_soap11, ah, submitnum);
				}
			}
		}
		catch(Exception ex)
		{
			System.err.println("main: " + ex);
		}
	}

	/**
	 * verifyCredentials
	 * 	calls the VerifyCredentials API in the SLIP webservice
	 *
	 * @param endpoint
	 *	either the SOAP 1.1 or the SOAP 1.2 service endpoint
	 * @param ah
	 *	authentication header with SLA Broker Number, Username and AMS Token
	 * @return boolean
	 *	true = successfully verified the credentials
	 *	false = verificatin of the credentials failed
	 */
	public static boolean verifyCredentials(AMSBatchFilingSoap endpoint,
			AuthenticationHeader ah)
	{
		boolean success = false;
		try
		{
			// execute the webservice
			VerifyCredentialsResult res = endpoint.verifyCredentials(ah);
			// check the response
			String code = res.getStatusCode();
			String text = res.getStatusMessage();
			System.out.println("VerifyCredentials: Code=" + code + 
				" Message=" + text);
			if(code.equals("1")) success = true;
		}
		catch(Exception ex)
		{
			System.err.println("VerifyCredentials: " + ex);
		}
		return success;
	}

	/**
	 * checkStatus
	 * 	calls the CheckStatus API in the SLIP webservice
	 *
	 * @param endpoint
	 *	either the SOAP 1.1 or the SOAP 1.2 service endpoint
	 * @param ah
	 *	authentication header with SLA Broker Number, Username and AMS Token
	 * @param submission_number
	 * 	Submission number of a previously uploaded batch
	 * @return String
	 *	Batch Status
	 */
	public static String checkStatus(AMSBatchFilingSoap endpoint,
			AuthenticationHeader ah, String submission_number)
	{
		String batch_status = "Unknown";
		try
		{
			// execute the webservice
			CheckStatusResult res = endpoint.checkStatus(submission_number,ah);
			// check the response
			String code = res.getStatusCode();
			String text = res.getStatusMessage();
			String submission = res.getSubmissionNumber();
			Status status = res.getStatus();
			ArrayOfSubmissionError errors = res.getErrors();
			System.out.println("CheckStatus: Code=" + code + 
				" Message=" + text);
			System.out.println("CheckStatus: Submission Number = " +
				submission);
			System.out.println("CheckStatus: Batch Status = " +
				status);
			// list error messages (if any)
			if(errors != null)
			{
				List<SubmissionError> errlist = errors.getSubmissionError();
				for(SubmissionError error : errlist)
				{
					String id = error.getElementId();
					ElementType typ = error.getElementType();
					String msg = error.getErrorMessage();
					System.out.println("Id: " + id + " Type: " + typ +
						" Error: " + msg);
				}
			}
			// if the API call was successful return the Batch Status
			if(code.equals("1"))
			{
				batch_status = status.value();
			}
		}
		catch(Exception ex)
		{
			System.err.println("CheckStatus: " + ex);
		}
		return batch_status;
	}

	/**
	 * getNotifications
	 * 	calls the GetNotifications API in the SLIP webservice
	 *
	 * @param endpoint
	 *	either the SOAP 1.1 or the SOAP 1.2 service endpoint
	 * @param ah
	 *	authentication header with SLA Broker Number, Username and AMS Token
	 * @param submission_number
	 * 	Submission number of a previously uploaded batch
	 */
	public static void getNotifications(AMSBatchFilingSoap endpoint,
			AuthenticationHeader ah, String submission_number)
	{
		try
		{
			// Type of Notifications requested
			Type typ = Type.fromValue("Alert");

			// execute the webservice
			GetNotificationsResult res = endpoint.getNotifications(submission_number, typ, ah);
			// check the response
			String code = res.getStatusCode();
			String text = res.getStatusMessage();
			String submission = res.getSubmissionNumber();
			NotificationStatus status = res.getNotificationStatus();
			ArrayOfNotification notices = res.getNotifications();
			System.out.println("GetNotifications: Code=" + code + 
				" Message=" + text);
			System.out.println("GetNotifications: Submission Number = " +
				submission);
			System.out.println("GetNotifications: Notification Status = " +
				status);
			// list notification messages (if any)
			if(notices != null)
			{
				List<Notification> notelist = notices.getNotification();
				for(Notification notice : notelist)
				{
					Type notetyp = notice.getType();
					String id = notice.getElementId();
					ElementType elemtyp = notice.getElementType();
					String msg = notice.getMessage();
					System.out.println("Notification Type: " + notetyp +
						" Id: " + id + " Type: " + elemtyp + " Note: " + msg);
				}
			}
		}
		catch(Exception ex)
		{
			System.err.println("GetNotifications: " + ex);
		}
	}

	/**
	 * uploadBatchFiling
	 * 	calls the UploadBatchFiling API in the SLIP webservice
	 *
	 * @param endpoint
	 *	the MTOM service endpoint
	 * @param ah
	 *	authentication header with SLA Broker Number, Username and AMS Token
	 * @param FileName
	 * 	Name of the SLIP Batch (ZIP-file) to be uploaded.
	 *
	 * @param PreviewDataInSLIP
	 *	Indicate whether to preview the data (true) or directly submit it (false).
	 * @return submission_number
	 *	Submission number if successfully submitted. Null if only uploaded or error.
	 */
	public static String uploadBatchFiling(AMSBatchFilingMtom endpoint,
			AuthenticationHeader ah, String FileName, boolean PreviewDataInSLIP)
	{
		String submission_number = null;
		try
		{
			// Create the data stream with the SLIP Batch Data
			UploadBatchFilingStream stream = new UploadBatchFilingStream();
			File batch = new File(FileName);
			if(batch.exists() && batch.isFile() && batch.canRead())
			{
				byte[] data = new byte[(int)batch.length()];
				FileInputStream fis = new FileInputStream(batch);
				fis.read(data);
				fis.close();
				stream.setData(data);
			}
			else
			{
				throw new Exception("Error: Argument '" + FileName +
					"' is not a file or cannot be read.");
			}

			// Additional header arguments for the webservice
			String Comments = "This is only a test. If it were a real batch it would have real data!";

			// execute the webservice
			UploadBatchFilingStreamResponse rsp = endpoint.uploadBatchFiling(stream, ah,
					Comments, FileName, PreviewDataInSLIP);

			// check the response
			UploadBatchFilingResult res = rsp.getUploadBatchFilingStreamResult();
			String code = res.getStatusCode();
			String text = res.getStatusMessage();
			String submission = res.getSubmissionNumber();
			System.out.println("UploadBatchFiling: Code=" + code + 
				" Message=" + text);
			System.out.println("UploadBatchFiling: Submission Number = " +
				submission);

			// if the upload was successful return the submission number
			if(code.equals("1"))
			{
				submission_number = submission;
			}
		}
		catch(Exception ex)
		{
			System.err.println("UploadBatchFiling: " + ex);
		}
		return submission_number;
	}
}
