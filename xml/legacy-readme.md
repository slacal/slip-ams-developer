# SLIP AMS & XML

[The Surplus Line Association of California (SLA California)](https://www.slacal.com/) offers a web-based application called the [Surplus Lines Information Portal (SLIP)](https://slip.slacal.com/) that allows brokers to electronically submit their policy information to the [SLA California](https://www.slacal.com/).

[SLIP](https://slip.slacal.com/) allows authorized brokers to electronically submit policy information in the following ways:

- **Manual Data Entry** - An authorized user can log into SLIP to manually enter policy information one record at a time.
- **Bulk Upload (XML)** -  An authorized user can log into SLIP to upload data for one or more policies.
- **AMS Upload** - An authorized user can generate and submit a batch file from their internal Agency Management System (AMS) using web services provided by SLIP.
- **Batch Image Upload** - An authorized user may submit a batch by uploading a document image file or files. Users will only get a confirmation that the file(s) were successfully uploaded, but no other batch information will be available in SLIP for that batch. This function is nearly identical to the old Broker Extranet method.

This repository covers the technical details on how to implement SLIP's **Bulk Upload (XML)** and **AMS Upload**.

## What Is Here?

You will find the following items in this repository:

- Code samples to help you get started using **AMS Upload**.
- Documentation on how to use **Bulk Upload (XML)** and **AMS Upload**, as well as details on how those processes work.
- Sample XML files to use as a reference when working with **Bulk Upload (XML)**.

| Document | Description |
|-|-|
| [Batch Submission Manual for Brokerages](lib/AutomatedBatchSubmissionManual.md) | The purpose of this document is to provide technical information on the SLA California electronic submission process. This document is intended for the technical resources who will work with the brokers to extract policy data from their data management system for the purpose of creating the submission to SLA California. Technical resources typically include system architects, designers, and developers with computer programming experience. It is assumed the reader of this document has an understanding of XML (Extensible Markup Language) and HTTP. |
| [Bulk Upload (XML) Schema (XSD)](lib/brokerage-xml-schema.xsd) | The XSD (XML Schema Definition) used to validate the uploaded XML. |
| [Bulk Upload (XML) Schema (XSD) Documentation](https://slacal.bitbucket.io/brokerage-xml-schema.xsd.html) | Details & descriptions for the XSD used to validate the uploaded XML. |
| [Example clients](https://bitbucket.org/slacal/slip-ams-client-sample/src/master/) | Example clients that demonstrate connecting to the SLIP AMS web services. |

## Who Is This For?

This repository is for developers and teams that wish to understand the details on how to electronically submit policy information to [SLA California](https://www.slacal.com/) using either **Bulk Upload (XML)** or **AMS Upload**.

## Questions & Help

For any questions or help, please contact [SLA California](https://www.slacal.com/) and [send us a message](https://www.slacal.com/send-us-a-message).

## 2024 SL2 Form Updates

As of January 1, 2024, the SLA is using a new format and layout for the SL2 Form. Please note that we are not updating the XML schema to reflect these changes. Users can continue to provide filings using the existing XML schema, and the data will be converted to the new SL2 Form format automatically. **No changes are required** to continue submitting the existing XML schema.

**If you wish to update your schema to reflect the new SL2 format, the SLA encourages you to update to the new JSON schema.** In addition to reflecting the updated SL2 format, the JSON schema provides a number of benefits not offered by the XML schema. Please see our Learning Center Resource page on the JSON Schema to find out more. If you would like more information regarding switching to the JSON schema, reach out to [support@slacal.com](mailto:support@slacal.com) and we will be happy to assist you.

## Maintained By

The code, samples, and documentation provided here are maintained by [SLA California](https://www.slacal.com/).
