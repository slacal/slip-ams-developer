# Technical Documentation - CheckTransactionStatus

## CheckTransactionStatus API
-	`CheckTransactionStatus()` API has been implemented in AmsBatchFiling.svc web service.
- The function will retrieve all the Transactions for a given submission number and broker number.
  
## CheckTransactionStatusRequest
The `CheckTransactionStatusRequest()` should be passed when calling CheckTransactionStatus API. The request should include the following fields:

**Request Parameters**:

| Parameter | Data Type | Description |
|-|-|-|
| AuthenticationHeader | String | It includes SLA Broker Number, User Name and API Key |
| SubmissionNumber | String | Submission Number for which list of transactions is requested |

## CheckTransactionStatusResponse
Web service will return following response which includes following information:

**Response Parameters**:

| Parameter | Data Type | Description |
|-|-|-|
| StatusCode | String | Indicates whether the credential has been successfully verified. The value `1` indicates success and `0` means failure. |
| StatusMessage | String | A message describing the status of the credential verification if any error occurred during processing: “`Method call successful`” if the credential has been verified successfully. "`No transactions found for <submissionnumber> and <brokernumber>`" if the transaction is not found. |
| TransactionDetails | List | List of transactions for given submission number and broker number. |

## TransactionDetail

| Parameter | Data Type | Description |
|-|-|-|
| TransactionID | integer | Represents Agent Transaction Id for the transaction |
| PolicyNumber | String | Represents Policy Number |
| TransactionType | String | Type of Transaction. Possible values: `New`, `Endorsement`, `Audit`, `Cancellation`, `Renewal`, `Extension` or `Offset`. |
| Premium | Decimal | Represents premium for the Transaction |
| TaxableFees | Decimal | Represents Taxable Fees for the Transaction |
| InsuredName | String | Represents Name of Insured |
| EndorsementNumber | String | Represents Endorsement Number |
| EffectiveDate | DateTime | Represents Date from which Transaction is Effective |
| Status | String | Status of Transaction. Possible values: `Pending`, `Registered`, or `Returned`. |
| ConfirmationNumber | String | Confirmation Number if provided on an individual Transaction |

