# Automated Batch Submission

### The Surplus Line Association of California | [Our Website](https://www.slacal.com)

##### Content updated April 30, 2015

---

## Table of Contents

1. Introduction
2. XML Batch Creation Guidelines
    * 2.1 Batch Definition
    * 2.2 SLA California Batch Filing Guidelines
        * 2.2.1 XML Batch File Size and Structure
        * 2.2.2 XML Batch File Name
        * 2.2.3 HTML (XML) Encoding
        * 2.2.4 Create an XML Batch File
        * 2.2.5 Initial XML Batch File Validation
        * 2.2.6 Secondary XML Batch File Validation
    * 2.3 Additional XML Information
3. Bulk Upload (XML)
    * 3.1 Description
    * 3.2 Prerequisites
    * 3.3 Process
        * 3.3.1 Create Batch File
        * 3.3.2 Log in to SLIP
        * 3.3.3 Upload and Submit the File
        * 3.3.4 SLIP Validates the File
        * 3.3.5 Monitor the Batch Submission Status
        * 3.3.6 Batch File is Accepted or Rejected
4. API Batch Submission (XML)
    * 4.1 Description
    * 4.2 Prerequisites
    * 4.3 Process
        * 4.3.1 Create Batch File
        * 4.3.2 Submit Batch File
        * 4.3.3 SLIP Validates the File
        * 4.3.4 Monitor the Batch Submission Status
        * 4.3.5 Review Notifications
    * 4.4 Methods
        * 4.4.1 Credential Verification Endpoint
        * 4.4.2 Upload Batch Filing Endpoint
        * 4.4.3 Check Status Endpoint
        * 4.4.4 Get Notifications Endpoint
5. Batch Image File (BIF) Upload (Non-XML)
    * 5.1 Description
    * 5.2 Prerequisites
    * 5.3 Process
        * 5.3.1 Create New Batch
        * 5.3.2 Log in to SLIP
        * 5.3.3 Upload and Submit BIF
        * 5.3.4 SLA Validates the File
6. Frequently Asked Questions

---

## 1. Introduction

SLIP (Surplus Lines Information Portal) is a portal that allows brokerages to submit policy data to
SLA California electronically. This document provides instructions for the automated submission
options available, through SLIP, to users of the SLA California system.
While brokerages will always be able to submit data manually into SLIP, bulk uploads of batches
streamline processes and save time. Brokerages using products that don’t integrate with SLIP
will have to enter data twice and track submissions separately.
There are three methods for automated bulk uploads via SLIP: Bulk upload (XML), Automated
Process Interface (API) submission, and Batch Image File (BIF) upload. The standard way to
automate batch submission is to export the policy data as XML so it can be uploaded manually
as a single ZIP file (Bulk Upload). You can take automation further by using API submission for
complete integration. Finally, you can upload a group of document images as a batch (BIF).
The remainder of this document summarizes these automated batch submission methods and
provides the technical details necessary to implement each method.  

## 2. XML Batch Creation Guidelines

If you decide to implement either the Bulk Upload or the API batch submission method there are
some common guidelines for automated batch submission. This section provides the common
guidelines and requirements for these automated batch submission methods. For details on the
Batch Image File upload process, please see section 5. below.

### 2.1 Batch Definition

In SLIP, a Batch refers to each group of policies submitted to the SLA for review. Any XML
file containing data for one or more policies is considered an XML batch file. The XML batch
file is combined with any additional policy documentation (electronic document images of
Declaration Pages, Syndicate List, SL Forms, etc.) into a single ZIP file for use in the Bulk
Upload method.

### 2.2 SLA California Batch Filing Guidelines

Brokerages should submit filings in batches of no more than 75 documents with a cover
sheet. If a brokerage’s filings exceed 100 items in a month, the brokerage should file
batches weekly or at least twice per month.  

The batch cover sheet should include the following information:

* Assigned SLA broker number
* Name of the insured
* Policy number
* Premium amount
* Stamping fee (for each item)
* Total number of items being filed
* The type of document being filed(new, renewal, endorsement, cancellation,
extension endorsement, or offset)  

Items in the batch cover sheet should be arranged as follows:

* Items should be listed in the same order as the documents in the batch
* The state surplus line tax and stamping fee must be shown separately on the filing
document.
* The invoice date must be included for each policy and endorsement filed.
* The date invoiced can be shown on your cover sheet or declarations page.
* A copy of the invoice may be attached to the policy or endorsement.

The documents in a batch should be submitted in the following order:

* New and renewal policies
* Endorsements
* Cancellation endorsements
* Non-money endorsements

Documents for each new or renewal policy should be submitted in the following
order:

* Declaration page
* Cover note or binder
* Forms or attachments to the policy (if applicable)
* SL-1 form
* SL-2 form (when applicable)

More information and details about specific types of filings can be found on the SLA
California website’s Filing Procedures section located online at
<https://www.slacal.com/brokers/broker-filing-procedures>.

#### 2.2.1 XML Batch File Size and Structure

The following guidelines apply to the XML Bulk Upload and the API Batch
Submission:

* The batch ZIP file cannot exceed 1 GB.
* The root of the ZIP file should contain one XML file and the policy documents
and/or folders containing policy documents associated with the filings in the
XML file. Policy documents and supporting documentation can be associated at
the batch level or at the transaction (policy) level within the XML.
* The XML file can contain an element that references individual file names or a
folder that can contain one or more files. (These elements in the XML file allow
SLIP to associate a file or group of files with either the batch as a whole, or with
a specific transaction.) Files that are saved directly in the batch ZIP file (a.k.a.
the “root”) can be referenced directly in the XML file by using the file name.
* Files that are saved to a folder within the batch ZIP file must be referenced by
the name of the folder in which they are located.  

Brokerages can name files or folders associated with the batch according to their
own business rules. The XML file can be created to include elements referencing a
specific file or folder name for a batch or for a transaction based on these rules at
the time the XML file is generated. For example, a brokerage may choose to have a
folder created for each transaction. The business rules may state that the folder will
be named based on the policy number and transaction identifier (or other identifier
relevant to the brokerage management system that allows a user to identify the
transaction). In this instance, the XML file would create an element for each
transaction where the referenced folder is named with a combination of policy
number and transaction ID (e.g., Folder Name = “PolicyNumber_TransactionID”).  

Each brokerage can determine its own process for naming files or folders and modify
the system to generate the XML file according to those rules. Selecting a naming
convention associated with the policy or transaction identifier may make it easier for
the user to identify a document's folder without having to review the XML file to find
the element. Each folder should be given a unique name.
The graphic below represents the concepts related to files and folders that are
referenced in the XML file:  

![Batch Zip File Diagram](/assets/images/batchzipfile.jpg "Batch Zip File")

* The batch ZIP file contains the XML file, and the referenced files or folders.
* There is an element at the batch level that references supporting
documentation. This element may reference individual files or a folder.
* There is an element for each transaction within the batch that references
supporting documentation. This element may reference individual files or a
folder.
* If supporting documentation is referenced by file name, it should be included
at the root level (directly inside the batch ZIP file). If files are saved to a folder
within the batch, the folder name should be used in the reference.
* Each folder name in the batch ZIP file should be unique (i.e., no two folders can
share the same name).  

#### 2.2.2 SML Batch File Name

The file name is limited to 200 characters. While there is no required naming
convention, we recommended that filenames should make maintaining and tracking
your submissions easy. We suggest that you include the submission date and time in
the file name. For example, 20100501_0930_Batch.zip (date_time_Batch.zip or
CCYYMMDD_HHMM_Batch.zip) would indicate the batch was created on 05/01/2010
at 9:30 AM.

#### 2.2.3 HTML (XML) Encoding

Several special characters are reserved and cannot be used directly in XML element
or attribute data. Replace them with XML Entity references or XML Encoded text.
These special characters act as flags to the parser; they delimit the document’s actual
content and tell the parser to take specific actions. These special characters,
therefore, must be represented in their encoded format:  

| Character Name | Reserved Character | Entity Reference |
| -------------- | :----------------: | :--------------: |
| Ampersand      | &                  | \&amp;           |
| Apostrophe     | '                  | \&apos;          |
| Quote          | "                  | \&quot;          |
| Less Than      | <                  | \&lt;            |
| Greater Than   | >                  | \&gt;            |

#### 2.2.4 Create an XML Batch File

The creation of the batch file will require the involvement of a technical resource that
is familiar with XML and the data management system in use by your brokerage.
Several different data management systems are used by brokerages throughout the
country; therefore, this document cannot provide step-by-step instructions on how
to extract policy data from your specific data management system. Rather, this
document identifies the structure and formatting requirements of the batch
submission in its final form.  

The first step in the creation of the batch file is to identify the criteria by which policy
data should be extracted from the brokerage’s data management system. Typically,
brokerages extract data based on a specified date range or some other criteria
indicating a submission to SLA California is required.  

Once the criteria to extract policy data is identified for your data management
system, a technical resource must create the XML file that contains the policy data.
For details on the required format and structure of the XML file, please refer to
Section 2 – Batch Creation Guidelines. An XML schema will be provided upon
request. The XML schema identifies technical constraints on the content and
structure of the XML file and can be used to validate the XML file prior to submission.  

Once the XML file is created and the policy documents have been identified and/or
extracted from the data management system, all of the files should be included in
one ZIP file for submission to SLA California.  

Note: The system will not accept Microsoft Excel files saved as XML Data or XML
Spreadsheet file types. Please follow the XML format described in this document and
identified within the XML Schema to create the XML file.

#### 2.2.5 Initial XML Batch File Validation

This section describes the first set of validations performed during the initial batch
submission. If the document fails ANY of the validations identified below, the ENTIRE
batch file will NOT be accepted and will not move on to step 2 of the initial validation.  

In the initial file validation process, SLIP completes the following steps:

* Parse the document and check that the document is well-formed.
* Check the XML file document against the XSD (XML Schema Definition) file.
* Check the length of all data elements to ensure they do not exceed maximum
lengths.
* Check that values of the specified elements comply with the detailed XML
document requirements and the XML schema.
* Check that the document file names (policy documentation) contained within
the ZIP file match those listed in the XML file.
* Check that all associated policy document types are PDF, PDF/A, PNG, TIFF, or
JPG/JPEG. Any other format will not be accepted.
* Check that each associated policy document (contained in the ZIP file) is less
than or equal to 150 MB.  

If any of the batch data is invalid, the system will reject the entire file. An email will be
sent to the SLIP user explaining the rejection and requesting resubmission. The user
must correct the batch and resubmit it through SLIP. If the data file is valid, the file
will be imported into a temporary queue for policy-level data validation.

#### 2.2.6 Secondary XML Batch File Validation

This section describes the secondary validation performed by SLIP after the initial
validation is passed. If the policy data in the XML file fails ANY of the following
validations, the ENTIRE batch file will NOT be accepted.  

In the secondary file validation process, SLIP completes the following steps:

1. Check for a valid broker or brokerage identification number. The brokerage
identification number is the SLA Broker Number for which the batch is being
submitted. The brokerage identification number is issued by SLA California.

2. Accept and/or reject the batch. An e-mail will be sent to the submission contact
to confirm the acceptance or rejection of the batch. If the batch has been
rejected, the message will contain a detailed description of the problem(s) and
instructions to correct and resubmit the batch. The user must correct the batch
and resubmit it through SLIP.  

### 2.3 Additional XML Information

XML creation software may help you examine and work within the parameters of the XML
schema. These tools include Liquid XML Studio, Stylus XML Studio, XML Spy, and others.
XML creation software will also validate your file prior to submission.  

The following websites contain valuable information regarding the XML Standard and the
UCC XML Standard as well as some information concerning XML tools.  

* <https://www.w3.org/XML>
* <http://www.xml.org>
* <https://www.xml.com>
* <http://www.w3schools.com/xml/default.asp>
* <http://www.w3schools.com/Schema/default.asp>

## 3. Bulk Upload (XML)

### 3.1 Description

The Bulk Upload method allows brokerages to submit policy and transaction data for
multiple policies at once. This process will especially benefit brokerages that file a large
amount of policy data with SLA California since a single XML file may contain information
for multiple policies.  

Brokerages that store data in a centralized data management system can make use of the
Bulk Upload method. The following list provides a high-level list of the steps contained
within the Bulk Upload process:  

1. The brokerage will generate an XML file containing the policy data it wishes to submit
to SLA California. Typically, the XML file will include policy data that was added or
modified within a specified date range or since the last XML batch submission.
2. The brokerage should also include any associated policy documentation (PDF, PDF/A,
PNG, TIFF, and JPG/JPEG) that is associated with the policy data contained within the
XML file. The policy documents will be referenced by filename within the XML file.
3. The brokerage will create a ZIP file containing the XML file and the associated policy
documentation (again, referenced by filename within the XML file).
4. The brokerage will log in to SLIP to upload the ZIP file containing the XML file and all
associated documents.  

### 3.2 Prerequisites

Brokerages may submit policy data using the Bulk Upload method if the following
requirements are met: A SLIP user account must exist for a submitting brokerage. SLA
California will create one master user account for each participating brokerage. The master
user within each brokerage will have the ability to create other user accounts for that
brokerage.  

A supported web browser must be used. The following web browsers will be supported:  

| Operation System      | Web Browser       | Version |
|-----------------------|-------------------|---------|
| **Micorosft Windows** | Internet Explorer | 8+      |
|                       | Firefox           | 3+      |
|                       | Safari            | 4.0.3+  |
|                       | Chrome            | 4.1+    |
| **Mac OS**            | Safari            | 4+      |
|                       | Firefox           | 3+      |

An SLA California Broker Number (SLA #) is required within the XML file for the submitting
brokerage. To identify your SLA #, please contact SLA California.

### 3.3 Process

This section identifies the steps required to create and submit policy information using the
manual batch file upload process. The graphic below is a high-level representation of the
process flow and indicates the systems involved.  

![Manual Batch File Upload Diagram](/assets/images/manualbatchfileupload.jpg "Manual Batch File Upload")  

#### 3.3.1 Create Batch File

The first step in the Bulk Upload process is to create the XML batch file with the
policy data. Refer to section 3.1.6 of this document for details about creating the XML
batch file.  

For the Bulk Upload process, the brokerage management system may be configured
to create the entire batch with all policy documentation already included in a ZIP file.
If it is not configured this way, additional steps may be taken by users to manually
add the desired policy documents to either the root of the ZIP file or a folder
referenced within the batch ZIP file. See section 2.2.1. Batch File Size and Structure
for more details about files and folders within the batch ZIP file.

#### 3.3.2 Log in to SLIP

Using a supported web browser, go to the SLA California SLIP website. Enter your
username and password. This will establish a secure connection and validate your
identity.  

SLA California will create one administrative user for each brokerage. The
administrative user for each brokerage will have the ability to create other users for
that brokerage.

#### 3.3.3 Upload and Submit the File

Go to the Batch Submission page in SLIP. Following the instructions on this page,
browse to and select the compressed ZIP file containing the XML file and associated
policy documents (PDF, PDF/A, PNG, TIFF, and JPG/JPEG). Submit the file for upload.  

SLIP will assign a Batch Source for all batches. Batches that are uploaded manually
will have “XML” listed as the batch source.  

![Create New XML Bulk Upload Batch](/assets/images/newxmlbulkbatch.jpg "Create New XML Bulk Upload Batch")  
*Figure 1 - Create New XML Bulk Upload Batch*  

![Select Zip XML File for Bulk Upload](/assets/images/selectxmlbulkbatch.jpg "Select Zip XML File for Bulk Upload")
*Figure 2 - Select Zip XML File for Bulk Upload*  

![Bulk Upload Zip XML File Saved](/assets/images/selectxmlbulkbatchsaved.jpg "Bulk Upload Zip XML File Saved")
*Figure 3 - Bulk Upload Zip XML File Saved*  

#### 3.3.4 SLIP Validate the File

After you successfully upload a batch file in SLIP, the system will queue the
submission for processing. When the system is ready to process the submission, the
validation process will begin.  

The first step is to validate the format and structure of the XML file as identified in
the XML Schema. This step must be successfully completed before any additional
processing can take place. If the initial validation process is successful, the XML file
will continue to the secondary validation process. The secondary validation process
validates the policy data contained within the XML file itself. If either validation
process is unsuccessful, the file will be rejected. The XML file format and/or data will
have to be corrected and resubmitted.  

Whether the file is accepted or rejected, an e-mail will be sent to the user or users
associated with the SLA broker number. If the submission was successful, the email will include the filing number and filing date. If this submission was rejected, the
email will contain the date and time the file import was attempted and the reason(s)
the file was rejected. In both scenarios, the Batch Submission page within SLIP will
display the processing status of any submission.

#### 3.3.5 Monitor the Batch Submission Status

After confirming that your batch file was successfully uploaded in SLIP, you may
monitor the batch progress in the SLIP Batch Submission page. The page will contain
the date the file was submitted and received by SLA California. Rejected submissions
should be corrected and resubmitted in a timely manner. The following table defines
the batch statuses.  

| External Status     | Description |
|---------------------|-------------|
| UPLOADED            | SLIP has identified and received a batch. The received batch will upload into SLIP automatically and be validated. |
| INVALID UPLOAD      | The batch has failed validation or was not properly imported into SLIP. SLA California has not accepted a batch. |
| PENDING USER ACTION | The batch is currently Saved in SLIP and requires user action before the batch will upload to SLA. |
| PENDING SLA REVIEW  | The batch was successfully uploaded to SLA California and is pending SLA review |
| CLOSED BY SLA       | The batch was invoiced by the SLA. *Note: Closed batches can contain open tags.* |
| RETURNED BY SLA     | The batch either was received and returned by the SLA or was not successfully uploaded. The SLA will notify users in either instance by both email and written letter. |

#### 3.3.6 Batch File is Accepted or Rejected

If the file has been accepted for import, no further action is required. As mentioned
in section 3.2.5, you may monitor the batch import process on the Batch Submission
page.  

If the file has been rejected for import, you must correct the issue(s) identified in the
rejection email and resubmit the batch file. If you have questions regarding batch file
rejection or resubmission, please contact SLA California.

## 4. API Batch Submission (XML)

### 4.1 Description

The API structure will provide the means for third party applications to submit insurance
policy data and documentation, on behalf of a broker or brokerage, to SLA California. It also provides the means for the AMS to receive feedback in regards to the acceptance of the
submitted data by the SLA.  

All endpoints will be based upon the hypertext transfer protocol (HTTP) and will use the
simple object access protocol (SOAP) to exchange structured data sets as defined in this
document.  

### 4.2 Pre-requisites

Brokerages may submit policy data using the API Batch Submission Method if the following
requirements are met:

1. For a brokerage management system to be allowed to interact with the API on behalf of a
user, it must first collect a set of credentials from the user. This set of credentials will
include a SLA broker number, username, and API key (user token) value. Users should be
able to obtain these values from their SLIP user profile page. The username and key pair
uniquely identify a user and indicate that users have granted the brokerage management
system permission to perform tasks on their behalf. See section 4.4.1, Credential
Verification Endpoint for the specific method used to verify the credentials.

    ![AMS Token](/assets/images/amstoken.jpg "AMS Token")
    *Figure 4 - AMS Token*  

2. The SLA broker number, username and API key values must be supplied within the SOAP
header with every request to the API. It is recommended that the brokerage management
system invoke the credential verification endpoint prior to submitting data to ensure that
the credentials are valid.

3. A SLIP user account must exist for a submitting brokerage. SLA California will create one
administrative user account for each participating brokerage. The administrative user in
each brokerage will have the ability to create other user accounts for that brokerage.

4. An SLA California Broker Number (SLA #) is required within the XML file for the submitting
brokerage. To identify your SLA #, please contact SLA California.

### 4.3 Process

This section identifies the steps required to create and submit policy information using the
API batch submission process. The high-level representation of the process flow below
indicates the systems involved.  

![API Batch Submission](/assets/images/apibatchsubmission.jpg "API Batch Submission")

#### 4.3.1 Create Batch File

The first step in the API batch submission process is to create the XML batch
file with the policy data. Refer to section 2.2.4. of this document for details
about creating the XML batch file. For the API batch submission process, the
brokerage management system will create the batch file automatically.

#### 4.3.2 Submit Batch File

Users will select the option in their brokerage management system to submit
the batch information to SLIP. See section 5.4.2, Upload Batch Filing
Endpoint for the specific method used to upload the file.  

SLIP will assign a Batch Source for all batches. Batches uploaded by the API
method will have “AMS” listed as the batch source.

#### 4.3.3 SLIP Validates the File

After you successfully upload a batch in SLIP, the system will queue the
submission for processing. When the system is ready to process the
submission, the validation process will begin.  

The first step is to validate the format and structure of the batch as identified
in the XML Schema. This step must be successfully completed before any
additional processing can take place. If the initial validation process is
successful, the batch will continue to the secondary validation process. The
secondary validation process validates the policy data contained within the
XML file itself. If either validation process is unsuccessful, the batch will be
rejected. The XML file format and/or data will have to be corrected and
resubmitted.  

Whether the file is accepted or rejected, an e-mail will be sent to the
submission contact identified within the batch. If the submission was
successful, the email will include the filing number and filing date. If this
submission was rejected, the email will contain the date and time the file
import was attempted and the reason(s) the file was rejected. In both
scenarios, the Batch Submission page within SLIP will display the processing
status of any submission. The user may also use the Check Status Endpoint
to get the status of the batch.

#### 4.3.4 Monitor the Batch Submission Status

After confirming that your batch was successfully uploaded in SLIP, you may
monitor the batch progress in the SLIP Batch Submission page, or use the
Check Status Endpoint method. The page will contain the date the batch was
submitted and received by SLA California. Rejected submissions should be
corrected and resubmitted in a timely manner. The following table defines
the batch statuses.  

See section 4.4.3 - Check Status Endpoint, for the specific method used to
monitor the batch status.  

| External Status     | Description |
|---------------------|-------------|
| UPLOADED            | SLIP has identified and received a batch. The received batch will upload into SLIP automatically and be validated. |
| INVALID UPLOAD      | The batch has failed validation or was not properly imported into SLIP. SLA California has not accepted a batch. |
| PENDING USER ACTION | The batch is currently Saved in SLIP and requires user action before the batch will upload to SLA. |
| PENDING SLA REVIEW  | The batch was successfully uploaded to SLA California and is pending SLA review |
| CLOSED BY SLA       | The batch was invoiced by the SLA. *Note: Closed batches can contain open tags.* |
| RETURNED BY SLA     | The batch either was received and returned by the SLA or was not successfully uploaded. The SLA will notify users in either instance by both email and written letter. |

#### 4.3.5 Review Notifications

The user will have the option to review the notifications generated by the
validation of the file. If the user has the appropriate permissions, they may
choose to submit the batch without correcting any alerts noted; however,
some users will not have this privilege and must correct the alerts before the
batch may be submitted to SLA California. See section 5.4.5 - Get Notifications
Endpoint, for the specific method used to retrieve the notifications.

### 4.4 Methods

This section contains the specific methods used in the API batch submission
method. Each method is referenced in a step of the API batch submission process,
above.

#### 4.4.1 Credential Verification Endpoint

Upon collecting API credentials from the user, it is recommended that the
AMS invoke this endpoint to verify access to the API on the user’s behalf. This
will ensure that the user’s account is active and verify the user’s identity.  

It is also recommended that the AMS verify the user’s credentials prior to
each data submission to ensure that the credentials remain valid.  

The following is a sample SOAP 1.1 request and response. The **placeholders**
shown need to be replaced with actual values.  

Request message:

    POST /AMSBatchFiling.asmx HTTP/1.1
    Host: serverhost
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    SOAPAction: "http://test.ws.slacal.org/ws-slip/VerifyCredentials"

    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
            <AuthenticationHeader xmlns="http://test.ws.slacal.org/ws-slip">
                <SLABrokerNumber>string</SLABrokerNumber>
                <UserName>string</UserName>
                <APIKey>string</APIKey>
            </AuthenticationHeader>
        </soap:Header>
        <soap:Body>
            <VerifyCredentials xmlns="http://test.ws.slacal.org/ws-slip" />
        </soap:Body>
    </soap:Envelope>

Response message:

    HTTP/1.1 200 OK
    Content-Type: text/xml; charset=utf-8
    Content-Length: length

    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <VerifyCredentialsResponse xmlns="http://test.ws.slacal.org/ws-slip">
                <VerifyCredentialsResult>
                    <StatusCode>string</StatusCode>
                    <StatusMessage>string</StatusMessage>
                </VerifyCredentialsResult>
            </VerifyCredentialsResponse>
        </soap:Body>
    </soap:Envelope>

Response parameters:

| Parameter     | Data Type | Description |
|---------------|-----------|-------------|
| StatusCode    | String    | Indicates whether the credential has been successfully verified. The value “1” indicates success and “0” means failure. |
| StatusMessage | String    | A message describing the status of the credential verification if any error occurred during processing. “Method call successful.” if the credential has been verified successfully. |

#### 4.4.2 Upload Batch Filing Endpoint

The AMS must prepare a batch ZIP file that will contain all relevant
information pertaining to the policies and transactions appropriate for batch
submission. The ZIP file will then be submitted to the Upload Batch Filing
endpoint. Upon completion of the batch filing, the API will provide the AMS
with a value that uniquely identifies the batch submission attempt
(submission number).  

The following is a sample SOAP 1.1 request and response. The placeholders
shown need to be replaced with actual values.  

Request message:

    POST /AMSBatchFiling.asmx HTTP/1.1
    Host: localhost
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    SOAPAction: "http://test.ws.slacal.org/ws-slip/UploadBatchFiling"

    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
            <AuthenticationHeader xmlns="http://test.ws.slacal.org/ws-slip">
                <SLABrokerNumber>string</SLABrokerNumber>
                <UserName>string</UserName>
                <APIKey>string</APIKey>
            </AuthenticationHeader>
        </soap:Header>
        <soap:Body>
            <UploadBatchFiling xmlns="http://test.ws.slacal.org/ws-slip">
                <FileName>string</FileName>
                <Data>base64Binary</Data>
                <Comments>string</Comments>
                <PreviewDataInSLIP>boolean</PreviewDataInSLIP>
            </UploadBatchFiling>
        </soap:Body>
    </soap:Envelope>

Request parameters:

| Parameter         | Data Type | Description |
|-------------------|-----------|-------------|
| FileName          | String    | The physical name of the file being submitted including file extension. |
| Data              | Binary    | The content of the policy submission as a base64Binary format. |
| Comments          | String    | Comments for the batch filing. |
| PreviewDataInSlip | Boolean   | Indicates whether user wants to check the policy data in SLIP before submission. |

Response message:  

    HTTP/1.1 200 OK
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    
    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <UploadBatchFilingResponse xmlns="http://test.ws.slacal.org/ws-slip">
                <UploadBatchFilingResult>
                    <StatusCode>string</StatusCode>
                    <StatusMessage>string</StatusMessage>
                    <SubmissionNumber>string</SubmissionNumber>
                </UploadBatchFilingResult>
            </UploadBatchFilingResponse>
        </soap:Body>
    </soap:Envelope>

Response parameters:

| Parameter         | Data Type | Description |
|-------------------|-----------|-------------|
| StatusCode        | String    | Indicates whether the credential has been successfully verified. The value “1” indicates success and “0” means failure. |
| StatusMessage     | String    | A message describing the status of the credential verification if any error occurred during processing. “Method call successful.” if the credential has been verified successfully. |
| Submission Number | String    | A value assigned for the batch submission. |

#### 4.4.3 Check Status Endpoint

The check status endpoint will allow the AMS to obtain the status of a batch
submission.  

The following is a sample SOAP 1.1 request and response. The **placeholders**
shown need to be replaced with actual values.  

Request message:

    POST /AMSBatchFiling.asmx HTTP/1.1
    Host: localhost
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    SOAPAction: "http://test.ws.slacal.org/ws-slip/CheckStatus"

    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
            <AuthenticationHeader xmlns="http://test.ws.slacal.org/ws-slip">
                <SLABrokerNumber>string</SLABrokerNumber>
                <UserName>string</UserName>
                <APIKey>string</APIKey>
            </AuthenticationHeader>
        </soap:Header>
        <soap:Body>
            <CheckStatus xmlns="http://test.ws.slacal.org/ws-slip">
                <SubmissionNumber>string</SubmissionNumber>
            </CheckStatus>
        </soap:Body>
    </soap:Envelope>

Request parameters:

| Parameter         | Data Type | Description |
|-------------------|-----------|-------------|
| Submission Number | String    | The submission number returned as the result of the batch submission. |

Response message:

    HTTP/1.1 200 OK
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    
    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <CheckStatusResponse xmlns="http://test.ws.slacal.org/ws-slip">
                <CheckStatusResult>
                    <StatusCode>string</StatusCode>
                    <StatusMessage>string</StatusMessage>
                    <SubmissionNumber>string</SubmissionNumber>
                    <Status>Uploaded or InvalidUpload or PendingUserAction or PendingSLAReview or Closed or Returned</Status>
                    <Errors>
                        <SubmissionError>
                            <ElementId>string</ElementId>
                            <ElementType>Policy or Transaction</ElementType>
                            <ErrorMessage>string</ErrorMessage>
                        </SubmissionError>
                        <SubmissionError>
                            <ElementId>string</ElementId>
                            <ElementType>Policy or Transaction</ElementType>
                            <ErrorMessage>string</ErrorMessage>
                        </SubmissionError>
                    </Errors>
                </CheckStatusResult>
            </CheckStatusResponse>
        </soap:Body>
    </soap:Envelope>

Response parameters:

| Parameter         | Data Type | Description |
|-------------------|-----------|-------------|
| StatusCode        | String    | Indicates whether the credential has been successfully verified. The value “1” indicates success and “0” means failure. |
| StatusMessage     | String    | A message describing the status of the credential verification if any error occurred during processing. “Method call successful.” if the credential has been verified successfully. |
| Submission Number | String    | A value assigned for the batch submission. |
| Status            | Enumberated Value | Identifies the status of the submission. The possible statuses are as follows: **Uploaded** – The status indicates either (1) SLIP has identified and received an XML or AMS batch. The received batch will upload into SLIP automatically, or (2) An XML or AMS batch has passed schema validation and is in the Importer Queue awaiting import. **Invalid Upload** – The status indicates either (1) An XML or AMS batch has failed schema validation and has been placed in the Responder Queue for notification, or (2) SLIP has failed to import a batch that had been accepted for import. EAS has not accepted a batch. **Pending User Action** – The batch was successfully uploaded to SLA and is pending SLA Review. **Pending SLA Review** – The status indicates either (1) The batch has been placed on the EAS Exporter Queue and will upload from SLIP to SLA, or, (2) The batch was successfully uploaded to SLA and is pending SLA Review. **Closed by SLA** – The Batch was invoiced by the SLA. (Note: Closed batches can contain Open Tags.) **Returned by SLA** – The batch either was received and returned by the SLA or was not successfully uploaded. The SLA will notify users in either instance by both email and written letter. |
| Errors            | Array     | When a batch filing is rejected, an array of errors will be provided containing the issues that occurred during processing. **SubmissionError**: Represents an error that occurred during processing. Each error identifies an element from the submitted data by custom element identifier and the element type (transaction or policy, for example) specified by the AMS. Batch filing errors are failures and cause the submission to be discarded. |

#### 4.4.4 Get Notifications Endpoint

During submission processing, notifications may be generated that can
provide the user with feedback or recommendations for elements within the
submitted data.  

The following is a sample SOAP 1.1 request and response. The **placeholders**
shown need to be replaced with actual values.

Request message:

    POST /AMSBatchFiling.asmx HTTP/1.1
    Host: serverhost
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    SOAPAction: "http://test.ws.slacal.org/ws-slip/GetNotifications"
    
    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
            <AuthenticationHeader xmlns="http://test.ws.slacal.org/ws-slip">
                <SLABrokerNumber>string</SLABrokerNumber>
                <UserName>string</UserName>
                <APIKey>string</APIKey>
            </AuthenticationHeader>
        </soap:Header>
        <soap:Body>
            <GetNotifications xmlns="http://test.ws.slacal.org/ws-slip">
                <SubmissionNumber>string</SubmissionNumber>
                <NotificationType>Alert</NotificationType>
            </GetNotifications>
        </soap:Body>
    </soap:Envelope>

Request parameters:

| Parameter         | Data Type        | Description |
|-------------------|------------------|-------------|
| Submission Number | String           | The submission number returned as the result of the batch submission. |
| NotificationType  | Enumerated Value | Allows the AMS to specify the type of notifications to be returned. |

Response message:

    HTTP/1.1 200 OK
    Content-Type: text/xml; charset=utf-8
    Content-Length: length
    
    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <GetNotificationsResponse xmlns="http://test.ws.slacal.org/ws-slip">
                <GetNotificationsResult>
                    <StatusCode>string</StatusCode>
                    <StatusMessage>string</StatusMessage>
                    <SubmissionNumber>string</SubmissionNumber>
                    <NotificationStatus>NotAvailable or AlertsExist or NoAlertsExist</NotificationStatus>
                    <Notifications>
                        <Notification>
                            <Type>Alert</Type>
                            <ElementId>string</ElementId>
                            <ElementType>Policy or Transaction</ElementType>
                            <Message>string</Message>
                        </Notification>
                        <Notification>
                            <Type>Alert</Type>
                            <ElementId>string</ElementId>
                            <ElementType>Policy or Transaction</ElementType>
                            <Message>string</Message>
                        </Notification>
                    </Notifications>
                </GetNotificationsResult>
            </GetNotificationsResponse>
        </soap:Body>
    </soap:Envelope>

Response parameters:

| Parameter          | Data Type | Description |
|--------------------|-----------|-------------|
| StatusCode         | String    | Indicates whether the credential has been successfully verified. The value “1” indicates success and “0” means failure. |
| StatusMessage      | String    | A message describing the status of the credential verification if any error occurred during processing. “Method call successful.” if the credential has been verified successfully. |
| Submission Number  | String    | A value assigned for the batch submission. |
| NotificationStatus | Enumerated Value | Indicates if notification is available depending on the batch status and if alerts exist when the notification is available. The possible notification statuses are as follows: **Not Available** – Alerts are not available now. The alerts will be available only when the batch status is Invalid Upload, Pending User Action, or Pending SLA Review. **Alerts Exist** – Alerts exist and the alerts will be listed in the array of notifications. **No Alerts Exist** - No alerts exist. |
| Notifications      | Array     | When alerts exist, an array of notifications will be provided based on what occurred during processing. **Notification** - Represents a notification entity (Alert) that occurred during processing. Each notification contains a message and fields necessary to identify the submitted data element with which the notification is associated. |

## 5. Batch Image File (BIF) Upload (Non-XML)

### 5.1 Description

The BIF Upload method allows brokerages to submit groups of document images.
This method does not include alerts prior to user submission. Instead, users get a
single email notification when a batch is submitted. BIF files are reviewed,
processed, and filed as in any other batch method. Acceptable file types include
PDF, PDF/A, PNG, TIFF, and JPG/JPEG.  

### 5.2 Pre-requisites

Brokerages may submit policy data using the BIF Upload method if the following
requirements are met:  

* A SLIP user account must exist for a submitting brokerage. SLA California will create
one master user account for each participating brokerage. The master user within
each brokerage will have the ability to create other user accounts for that
brokerage.

### 5.3 Process

This section identifies the steps required to create and submit policy information
using the batch image file (BIF) submission process. The high-level representation of
the process flow below indicates the systems involved.

![Bulk Image File Upload](/assets/images/batchimagefileupload.jpg "Bulk Image File Upload")

#### 5.3.1 Create New Batch

The first step in the BIF submission process is to gather the policies that will
be in the batch and create images files. Scan the policies within your batch to
a PDF format. You may scan policies into separate files, or consolidate your
batch into a single PDF.

#### 5.3.2 Log in to SLIP

Using a supported web browser, go to the SLA California SLIP website. Enter
your username and password. This will establish a secure connection and
validate your identity.

#### 5.3.3 Upload and Submit BIF

From the SLIP home screen, click the Upload Batch Image File link. Enter the
necessary details on the Create New Batch Wizard. From the Upload
Documents to Batch panel, select the appropriate document type and click
Browse. In the provided window, select the PDF batch file from your
computer and click the Open button. Click the Upload button and save. Once
the batch is saved, select Submit.

![Create New Batch: Upload Batch Image File](/assets/images/uploadbifbulkbatch.jpg "Create New Batch: Upload Batch Image File")
*Figure 5 - Create New Batch: Upload Batch Image File*

![Create New Batch Wizard](/assets/images/createnewbatchwizard.jpg "Create New Batch Wizard")
*Figure 6 - Create New Batch Wizard*

![View Batch and Submit](/assets/images/viewbatchsubmit.jpg "View Batch and Submit")
*Figure 7 - View Batch and Submit*

#### 5.3.4 SLA Validates the File

After you successfully upload a batch in SLIP, the system will queue the
submission for processing. When the system is ready to process the
submission, the validation process will begin.  

After submission, an e-mail will be sent to the submission contact identified
with the batch. With the BIF method, the Data Analysis Department at the
SLA will review the submission, but there will be no batch status updates
issued.

## 6. Frequently Asked Questions

The following list identifies frequently asked questions from technical resources
concerning the XML Batch Upload:  

1. Do I need a SLIP account to submit a Batch file?  
**Answer**: Yes, a SLIP account is required to submit policy data. If you are using the
API method, you also must submit the SLA Broker Number and API key value in the
SOAP header.

2. What is the user token? How can I generate a token?  
**Answer**: The user token is a unique identifier that is created for a particular user.
Users will log into SLIP to generate a token for their user account.

3. Can I use Excel to export a file to Batch?  
**Answer**: The data contained within a batch submission must be in XML format. XML
is a different way of storing data than Excel. XML is the leading standard for data
exchange, providing several inherent benefits including data validation, structural
enforcement, and platform independence. Please work with your technical staff to
prepare your file appropriately.

4. What is the “SLABrokerNumber” element in the SOAP Header and XML Schema?  
**Answer**: The Brokerage Number element refers to a unique identification number
that is assigned to each brokerage by the Surplus Line Association of California.

5. What is the purpose of the “XML_TransactionId” in the transaction element of the XML Batch Upload Method?  
**Answer**: The Transaction ID is a unique alphanumeric value provided by the
brokerage data management system and used to uniquely identify a policy
transaction submitted using the manual batch file upload.

6. How can I generate a batch file from our data management system?  
**Answer**: You will need to work with your IT staff to identify the best method to
export data from your data management system in the required format.

7. Can I use the manual batch file upload method and also use the manual data entry method?  
**Answer**: Yes, the system can handle this, but it is recommended you use only one
method to avoid the possibility of duplicating filing submissions.

8. Can I edit a policy transaction that has been submitted through the manual batch file upload method?  
**Answer**: Yes, you can edit transactions in SLIP that were submitted through the
manual batch file upload method.

9. How often can I upload a batch?  
**Answer**: There is no restriction on how often a batch may be uploaded. However, the Surplus Line Association of California does request that organizations with high volume please communicate with the Surplus Line Association of California to determine an appropriate rate of submission that will ensure a timely examination process.
