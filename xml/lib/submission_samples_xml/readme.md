# Sample XML Submissions

### The Surplus Line Association of California | [Our Website](https://www.slacal.com)

##### Content updated September 17, 2020

---

## Contents

The contents of this folder are sample "submission" files for testing the AMS Submission Import process.
All of these samples will import correctly into SLIP unless otherwise stated.

| Folder Name        | Description        |
| :----------------- | :----------------- |
| NoAlerts           | A simple example submission document that should import and have no alerts. Contains one basic policy with some coverages and fees. |
| MultiLevelLayering | An example submission document,containing one basic policy with some multi-level layering and SL2 admitted companies. |
| MultistateNRRA     | An example of a submission document with a multi state policy, including NRRA fields. |
| InvalidXML         | An example submission document similar to NoAlerts but is missing a field and will fail validation. |
